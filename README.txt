Librivox Import

============

A Librivox feed parser for Drupal, extending the Feeds modules.

Features
========

- Imports Librivox XML audibook data from Librivox, by book ID, to the import
  node type "Librivox Audiobook" using URLs like the following, where the final
  element is the audiobook ID number:

  https://librivox.org/api/feed/audiobooks/id/2343

- There is unfortunately, no master index of audiobook IDs.
  In order to identify the book you want, search the Librivox site,
  then click the RSS feed link on the book's RSS feed page, which contains 
  the book ID as its final element.

  Page for: Le P�re Goriot

  	https://librivox.org/rss/2343  (take only the number from this feed)
    	
- Go to the import page: http:/[www.yoursite.com]/import
- Select the "Librivox Audiobook Feed" feeds importer
- Enter the XML book feed URL with the id at the end in the
  "Import URL" field

  https://librivox.org/api/feed/audiobooks/id/2343

- XPath variables are configured for the standard Librivox audiobook data entry, but only
  five fields are currently generated in the import node:

	title
	first_name
	last_name
	description
        url_zip_file

  The XPath context is currently set at //books but could be set at //book .
	
  If you discover problems with the feed, you may have an unorthodox entry and
  need to override the XPath selectors for particular items in the "XPath Parser Settings"
  section of the import form.

  If you enter the audiobook feed URL directly in your browser, you will see the 
  item's XML data structure.

Requirements
============

- Drupal 7.x

- Job Scheduler
  http://drupal.org/project/job_scheduler
- Feeds
  http://drupal.org/project/feeds
- CTools
  http://drupal.org/project/ctools
- Feeds XPathParser
  http://drupal.org/project/feeds_xpathparser
- Patch Feeds to turn off Certificate checking; there is nothing critical
  involved in the data being grabbed by Feeds that needs SSL encryption.  The
  alternative is to save the certificate locally and reference it locally?

  The fix is at line 182 of "feeds/libraries/http_request.inc":

    // Fix invalid or misplaced certificate error
      
    // if ($accept_invalid_cert) {  // Act as if $accept_invalid_cert is TRUE
  
         curl_setopt($download, CURLOPT_SSL_VERIFYPEER, 0); // Do not verify

    // }

- Patch comment_node_type_delete($info) in core comments.module to prevent
  error messages during uninstall.
  SEE https://drupal.org/comment/4771916#comment-4771916
  $info object is sometimes missing, which the function does not account for

  function comment_node_type_delete($info) {
    $type = _node_extract_type($info); // added line
    // field_attach_delete_bundle('comment', 'comment_node_' . $info->type);
    field_attach_delete_bundle('comment', 'comment_node_' . $type);  // fixed
    $settings = array(
      'comment',
      'comment_default_mode',
      'comment_default_per_page',
      'comment_anonymous',
      'comment_subject_field',
      'comment_preview',
      'comment_form_location',
    );
    foreach ($settings as $setting) {
      // variable_del($setting . '_' . $info->type);
      variable_del($setting . '_' . $type); // fixed
    }
  }
  

  Follow XPath FeedsParser information for more complex configuration.


Installation
============

- Install Feeds and Import Librivox.  Required modules should be identified.

Configuration
=============

  Permissions

  - Feeds and Librivox Import set up the following permissions, which need to be configured
    (normally for admins, or only superadmin 1 will have permissions):
 
      Administer Feeds 
      Import Librivox Feed feeds 
      Delete items from Librivox Feed feeds 
      Unlock imports from Librivox Feed feeds (If a feed importation breaks for 
              some reason, users with this permission can unlock them.)
  -   You may have to set display weights for the field maintenance and display forms
      of the "Librivox Audiobook" import node.

Uninstallation
==============

- Uninstallation is not currently well configured, since the order of deleting items
  was causing fatal errors.
- Manual uninstall requires deleting the following items from the database:

Custom Librivox Audiobook node fields:

    TABLE field_config

      FIELD librivox_first_name
      FIELD librivox_last_name
      FIELD librivox_url_zip_file

    TABLE field_config_instance

      FIELD librivox_first_name
      FIELD librivox_last_name
      FIELD librivox_url_zip_file

    TABLE node_type

      NAME: Librivox Audiobook   (base: librivox_audiobook)

    TABLE feeds_source
 
      ID: librivox_audiobook_feed

    TABLE node (optional)

      Any imported node items you wish to delete

    TABLE node_revision

      Any imported node items you wish to delete

    TABLE feeds_item  (optional)

      This supposed to be the hash of imported items that enables Feeds to tell 
      whether something is supposed to be updated rather than a new item being
      created.  But this doesn't seem to be working.  Everything is getting imported
      new. "librivox_audiobook_feed" is the importer for items imported by this
      module.

    TABLE feeds_log  (optional)

      Any log data you wish to delete.  "librivox_audiobook_feed" is the name 
      of the feed that creates data for this module.


Custom Librivox Audiobook field data tables

    TABLE field_data_librivox_first_name
    TABLE field_data_librivox_last_name
    TABLE field_data_librivox_url_zip_file
    TABLE field_revision_librivox_first_name
    TABLE field_revision_librivox_last_name
    TABLE field_revision_librivox_url_zip_file

Clear cache before reinstalling:

   Drush cc all



